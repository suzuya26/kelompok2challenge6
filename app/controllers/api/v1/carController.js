const carService = require("../../../services/carService");

module.exports = {
    carsForm(req,res){
        res.render("carsform")
    },
    createCar(req,res){
    const car_name = req.body.car_name;
    const car_price = req.body.car_price;
    const car_type = req.body.car_price;
    const car_image = req.file.filename;
    const created_by =req.body.created_by;

    carService.createCar({ car_name, car_price, car_type, car_image ,created_by})

    res.redirect("/cardashboard");
    },
    listSemuaCar(req,res){
        carService.listCar()
        .then(({data_car}) => {
              res.render("cardashboard",{
                  data_car,
              })
          })
    }

}