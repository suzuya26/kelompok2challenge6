const carRepository = require("../repositories/carRepository")

module.exports = {
    async createCar(requestBody) {
        return carRepository.createCar(requestBody);
    },

    async updateCar(id, requestBody) {
        return carRepository.updateCar(id, requestBody);
    },

    async deleteCar(id) {
        return carRepository.delete(id);
    },

    async getCar(id) {
        return carRepository.findCar(id);
    },

    async listCar() {
        try {
            const car = await carRepository.findAllCar();

            return {
                data_car: car,
            };
        } catch (err) {
            throw err;
        }
    },
};
