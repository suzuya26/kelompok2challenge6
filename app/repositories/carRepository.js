const { Car } = require("../models")

module.exports = {
    createCar(createArgs){
        return Car.create(createArgs)
    },
    updateCar(id, updateArgs) {
        return Car.update(updateArgs, {
            where: {
                id,
            },
        });
    },
    deleteCar(id) {
        return Car.destroy({where: {id}});
    },
    findCar(id) {
        return Car.findByPk(id);
    },
    findAllCar() {
        return Car.findAll();
    },
}